mkdir ./small
for file in ./*.jpg
do 
	convert $file -resize 50% -define jpeg:extent=0.3MB ./small/$file
done

# Comments
# Line 1 creates a sub-directory called small
# Line 2 lists all the files in the working directory (make sure it only has jpgs)
# Line 4 makes a copy of Line 2 files in small that are less than 0.5 MB in size
