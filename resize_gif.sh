#!/bin/bash
mkdir ./small
for f in *.JPG
do
    convert $f -resize 10% -define jpeg:extent=0.1MB ./small/$f
done

convert -delay 30 -loop 0 ./small/*.JPG ./small/anim.gif
