#!/bin/bash
find . -name '*.wav' |
while read file # eg stuff/artist/album/title.wav
do      file="$PWD/${file#./}" # make absolute to get more info
        album=${file%/*}    # stuff/artist/album
        artist=${album%/*}  # stuff/artist
        album=${album##*/}  # album
        artist=${artist##*/} # artist
        title=${file##*/}   # title.wav
        title=${title%.wav} # title
        flac -s --best --delete-input-file \
         --tag="TITLE=$title" \
         --tag="ALBUM=$album" \
         --tag="ARTIST=$artist" \
         "$file" # creates .flac removes .wav
done
